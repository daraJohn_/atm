﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine_Demo
{
    class Application
    {
        public static void run()
        {
            Console.WriteLine("Welcome");
            Console.WriteLine("enter your pin");
            var Pin = Console.ReadLine();

            while(Pin != User.Pin || String.IsNullOrWhiteSpace(Pin))
            {
                Console.WriteLine("incorrect pin, reenter your pin");
                Pin = Console.ReadLine();


            }

            Console.WriteLine("choose your preferred language, 1 for English, 2 for Pidgin");
           var language = Console.ReadLine();



            while (language != "1" && language != "2")
            {
                Console.WriteLine("Invalid input, chose between 1 and 2");
                language = Console.ReadLine();
            }
            if (language == "1")
            {
                English.process();

                English.Remark();
                English.Rerun();
                English.withdrawal();
            }
            if (language == "2")
            {
                Pidgin.process();
                
                Pidgin.Remark();
                Pidgin.Rerun();
                Pidgin.withdrawal();
            }
        }
        
        
        /*
        public static void process()
        {
            
            Atm myAtm = new Atm();

            Remark();

            var choice = Console.ReadLine();

            while(choice != "1" && choice != "2")
            {
                Console.WriteLine("Invalid input, chose between 1 and 2");
                choice = Console.ReadLine();
            }

            switch (choice)
            {
                case "1":
                    Console.WriteLine("PROCESSING...");
                    Console.WriteLine($"Your account balance is {User.AccountBalance}");
                    Rerun();
                    break;

                case "2":
                    withdrawal();
                    Rerun();
                    break;
            }
        }

        public static void Remark()
        {
            Console.WriteLine("Welcome to C# ATM SERVICES");
            Console.WriteLine("1.To check balance");
            Console.WriteLine("2.To withdraw money");
            Console.WriteLine(" Enter your Transaction choice");
        }

        public static void Rerun()
        {
            var check = "1";

            Console.WriteLine("do you want to perform any other transaction? 1 for yes , 2 for no");
            check = Console.ReadLine();
            if (check == "1")
            {
                Remark();
                process();
            }
            else
            {
                Console.WriteLine("take your card");
            }
           
        }
        public static void withdrawal()

        {
            Console.WriteLine("Enter Amount To Withdraw");

            int withdrawal = Convert.ToInt32(Console.ReadLine());

            Atm myAtm = new Atm();
            myAtm.AmountLoaded -= withdrawal;

            while (withdrawal % 500 != 0 || withdrawal % 1000 != 0)
            {
                Console.WriteLine("Enter denomination in multiples of 500 or 1000");
                withdrawal = Convert.ToInt32(Console.ReadLine());
            }

            if (withdrawal <= User.AccountBalance && (withdrawal % 500 == 0 || withdrawal % 1000 == 0) && withdrawal <= myAtm.AmountLoaded)
            {
                Console.WriteLine("PROCESSING ......");
                Console.WriteLine("You can now take your cash");
                User.AccountBalance -= withdrawal;
                Console.WriteLine($" THE MONEY REMAINING IN THE USERS ACCOUNT AT THIS POINT IS {User.AccountBalance}");
               
                Console.WriteLine($" THE MONEY REMAINING IN THE ATM AT THIS POINT IS {myAtm.AmountLoaded} ");            
            }
            else if (myAtm.AmountLoaded == 0)
            {
                Console.WriteLine("Unable to Dispense Cash");
            }
            else if (withdrawal > User.AccountBalance)
            {
                Console.WriteLine("Insufficient Account Balance!!!");
            }
        

        }
    */
    }
    }

